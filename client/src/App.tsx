import { useEffect, useState } from "react";
import "./App.css";
import { fetchServerMessage, fetchServerPosts } from "./api";
import { serverUrl } from "./settings";

interface IServerHomeRes {
  message: string;
}

interface IPost {
  id: number;
  title: string;
}

console.log("Server URL is", `[${serverUrl}]`);

function App() {
  const [message, setMessage] = useState<IServerHomeRes | null>(null);
  const [errors, setErrors] = useState<Error[]>([]);
  const [posts, setPosts] = useState<IPost[] | null>(null);

  useEffect(() => {
    fetchServerMessage()
      .then(setMessage)
      .catch((err) => setErrors((prev) => [...prev, err]));
    fetchServerPosts()
      .then(setPosts)
      .catch((err) => setErrors((prev) => [...prev, err]));
  }, []);

  return (
    <>
      <h1>Vite + React</h1>
      {errors.length > 0 &&
        errors.map((error, i) => (
          <p key={i} className="ErrorMessage">
            {error.message}
          </p>
        ))}
      {message && (
        <p>
          OK response from server: <code>{message.message}</code>
        </p>
      )}
      {posts && posts.map((post) => <p key={post.id}>{post.title}</p>)}
    </>
  );
}

export default App;
