import { serverUrl } from "./settings";

export async function fetchServerMessage() {
  const response = await fetch(`${serverUrl}/api/message`);
  const data = await response.json();
  return data;
}


export async function fetchServerPosts() {
  const response = await fetch(`${serverUrl}/api/posts`);
  const data = await response.json();
  return data;
}