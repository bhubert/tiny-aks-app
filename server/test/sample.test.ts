import assert from "assert";

describe("Unit Tests", () => {
  describe("Sample Test", () => {
    it("should test that 1 + 1 === 2", () => {
      assert.strictEqual(1 + 1, 2);
    });
  });
});
