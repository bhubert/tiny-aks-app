import express, { Application } from "express";
import cors from "cors";
import morgan from "morgan";
import { commitSha } from "./settings";

const app: Application = express();
app.use(cors());
app.use(morgan("dev"));
app.use(express.json());

app.get("/", (req, res) => {
  res.send({ message: "Hello World!" });
});

app.get("/api/message", (req, res) => {
  res.send({ message: `Hello from server@${commitSha}` });
});


app.get("/api/posts", (req, res) => {
  const fakePosts = [
    { id: 1, title: "Azure AKS is awesome" },
    { id: 2, title: "My cluster is running" },
  ]
  res.send(fakePosts);
});

app.get("/health", (req, res) => {
  const appVersion = process.env.APP_VERSION || "0.1.0";
  res.send({ status: "ok", version: appVersion, commitSha });
});

export default app;
