#!/bin/bash

echo "ACI_NAME: $AZ_ACI_NAME"
echo "Resource Group: $AZ_RESOURCE_GROUP"

# Check if the container exists
az container show --name $AZ_ACI_NAME --resource-group $AZ_RESOURCE_GROUP
if [ $? -eq 0 ]; then
  echo "Container exists"
  # In any case, we want to restart the container

  az container restart --name $AZ_ACI_NAME --resource-group $AZ_RESOURCE_GROUP

  # # Check if the container is running
  # running=$(az container show --name $AZ_ACI_NAME --resource-group $AZ_RESOURCE_GROUP --query 'instanceView.currentState.state' --output tsv)
  # if [ "$running" == "Running" ]; then
  #   echo "Container is running"
  #   # Stop the container
  #   az container stop --name $AZ_ACI_NAME --resource-group $AZ_RESOURCE_GROUP
  # fi

  # # Delete the container
  # echo "Deleting container..."
  # az container delete --name $AZ_ACI_NAME --resource-group $AZ_RESOURCE_GROUP --yes

# If it doesn't exist, say so
else
  echo "Container does not exist"
  # Create container
  echo "Creating container..."
  az container create --resource-group $AZ_RESOURCE_GROUP --name $AZ_ACI_NAME --image $DOCKER_HUB_USERNAME/tiny-ts-express-app:$CI_COMMIT_REF_NAME --environment-variables COMMIT_SHA=$CI_COMMIT_SHA LOGGER_ACCESS_KEY=$LOGGER_ACCESS_KEY LOGGER_URL=$LOGGER_URL --dns-name-label aci-demo-bhubr --ports 80
fi
